package footballTeam;

public class StartTeam {
    public static void main(String[] args) {


        Goalkeeper goalkeeper = new Goalkeeper("Keylor Navas", 18_000_000, true, true);
        Goalkeeper substituteGoalkeeper = new Goalkeeper("Thibaut Courtois", 65_000_000, false, true);

        Backer leftBacker = new Backer("Marcelo", 60_000_000, true);
        Backer centralBacker = new Backer("Sergio Ramos", 45_000_000, true);
        Backer centralBacker1 = new Backer("Raphael Varane", 80_000_000, true);
        Backer rightBacker = new Backer("Daniel Carvajal", 60_000_000, true);
        Backer substituteBacker = new Backer("Nacho Fernandez", 35_000_000, false);

        Halfbacker leftHalfbacker = new Halfbacker("Luka Modric", 25_000_000, true);
        Halfbacker centralHalfbacker = new Halfbacker("Toni Kroos", 80_000_000, true);
        Halfbacker centralHalfbacker1 = new Halfbacker("Isco", 75_000_000, true);
        Halfbacker rightHalfbacker = new Halfbacker("Marco", 75_000_000, true);
        Halfbacker substituteHalfbacker = new Halfbacker("Gareth Bale", 90_000_000, false);

        Forward leftForward = new Forward("Karim Benzema", 40_000_000, true);
        Forward rightForward = new Forward("Mariano Díaz", 22_000_000, true);
        Forward substituteForward = new Forward("Vinicius Junior", 25_000_000, false);

        FootballPlayer[] footballPlayers = new FootballPlayer[15];
        footballPlayers[0] = goalkeeper;
        footballPlayers[1] = substituteGoalkeeper;
        footballPlayers[2] = leftBacker;
        footballPlayers[3] = centralBacker;
        footballPlayers[4] = centralBacker1;
        footballPlayers[5] = rightBacker;
        footballPlayers[6] = substituteBacker;
        footballPlayers[7] = leftHalfbacker;
        footballPlayers[8] = centralHalfbacker;
        footballPlayers[9] = centralHalfbacker1;
        footballPlayers[10] = rightHalfbacker;
        footballPlayers[11] = substituteHalfbacker;
        footballPlayers[12] = leftForward;
        footballPlayers[13] = rightForward;
        footballPlayers[14] = substituteForward;

        System.out.println("Main team:");

        MainTeamOrSubstitute mainTeamOrSubstitute = new MainTeamOrSubstitute();
        mainTeamOrSubstitute.mainTeam(footballPlayers);

        System.out.println("Substitute team:");

        mainTeamOrSubstitute.substituteTeam(footballPlayers);
    }
}
