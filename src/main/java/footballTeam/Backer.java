package footballTeam;

public class Backer extends FootballPlayer {

    public Backer(String name, int price, boolean team){
        this.name = name;
        this.contractPrice = price;
        this.substitute = team;
    }

}
