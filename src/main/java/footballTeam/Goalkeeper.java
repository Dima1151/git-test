package footballTeam;

public class Goalkeeper extends FootballPlayer {

    public Goalkeeper(String name, int price, boolean team, boolean hands){
        this.name = name;
        this.contractPrice = price;
        this.substitute = team;
        this.playHands = hands;
    }

}
