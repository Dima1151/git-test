package footballTeam;

public class MainTeamOrSubstitute {

    //method return a player in the team
    public void mainTeam(FootballPlayer[] footballPlayers){
        for (int i = 0; i < footballPlayers.length; i++) {
            if(footballPlayers[i].substitute){
                footballPlayers[i].showinfo();
            }
        }
    }

    //method return substitute player
    public void substituteTeam(FootballPlayer[] footballPlayers){
        for (int i = 0; i < footballPlayers.length; i++) {
            if(!footballPlayers[i].substitute){
                footballPlayers[i].showinfo();
            }
        }
    }

}
