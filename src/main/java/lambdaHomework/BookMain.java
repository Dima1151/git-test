package lambdaHomework;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

public class BookMain {

    public static void main(String[] args) {

        List<Book> library = new ArrayList<>();

        library.add(new Book("title1", new Author("Eric", 22, Gender.MALE)));
        library.add(new Book("title2", new Author("Alla", 35, Gender.FEMALE)));
        library.add(new Book("title3", new Author("Allan", 55, Gender.MALE)));
        library.add(new Book("title4", new Author("Allan", 57, Gender.MALE)));
        library.add(new Book("title5", new Author("Emma", 23, Gender.FEMALE)));
        library.add(new Book("title6", new Author("Sharlota", 54, Gender.FEMALE)));
        library.add(new Book("title7", new Author("Melan", 35, Gender.MALE)));
        library.add(new Book("title8", new Author("Lizy", 75, Gender.FEMALE)));

        List<Author> females = library.stream()
                .map(Book::getAuthor)
                .filter(author -> author.getGender() == Gender.FEMALE)
                .collect(Collectors.toList());

        OptionalDouble averange = library.stream()
                .map(Book::getAuthor)
                .filter(author -> author.getGender() == Gender.FEMALE)
                .mapToInt(Author::getAge)
                .average();

        List<String> males = library.stream()
                .map(book -> book.getAuthor())
                .filter(author -> author.getGender() == Gender.MALE && author.getAge() >= 30)
                .map(Author::getSurname)
                .map(String::toUpperCase)
                .distinct()
                .collect(Collectors.toList());

        System.out.println("Women's names: " + females);
        System.out.println("Average age of females: " + averange.getAsDouble());
        System.out.println("Men's names: " + males);
    }

}
