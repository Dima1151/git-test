package lambdaHomework;

import java.util.Objects;

public class Author {

    private Gender gender;
    private Integer age;
    private String surname;

    public Author(String surname, Integer age, Gender gender) {
        this.gender = gender;
        this.age = age;
        this.surname = surname;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return age == author.age &&
                gender == author.gender &&
                Objects.equals(surname, author.surname);
    }

    @Override
    public int hashCode() {

        return Objects.hash(gender, age, surname);
    }

    @Override
    public String toString() {
        return surname + "|" + age + "|" + gender;
    }

}
