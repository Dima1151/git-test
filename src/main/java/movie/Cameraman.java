package movie;

public class Cameraman extends MovieCrewMembers implements CameraUser {

    String cameraGenre;

    public String getCameraGenre() {
        return cameraGenre;
    }

    public void setCameraGenre(String cameraGenre) {
        this.cameraGenre = cameraGenre;
    }

    @Override
    public void participateInMovie(Movie movie) {
        if(movie.getGenre() == this.getCameraGenre()){
            System.out.println("Movie will be operated by: " + getFirstName() + " " + getLastName());
            useCamera();
        }
    }

    @Override
    public void useCamera() {
        System.out.println("Start using camera!");
    }

}