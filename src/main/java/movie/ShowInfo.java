package movie;

import java.util.Scanner;

import static movie.Messages.*;

public class ShowInfo {

    Scanner scanner = new Scanner(System.in);
    String st;

    //Method show information about directors in MovieSet;
    public void showDirectorInfo(Director[] directors){
        for (int i = 0; i < directors.length; i++) {
            System.out.println(directors[i].getFirstName() + " " + directors[i].getLastName() + " - " + directors[i].getGenre());
        }
    }

    //Method show information about actors in MovieSet;
    public void showActorInfo(Actor[] actors){
        for (int i = 0; i < actors.length; i++) {
            System.out.println(actors[i].getFirstName() + " " + actors[i].getLastName() + " - " + actors[i].getGenre());
        }
    }

    //Method show information about movie in MovieSet;
    public void showMovieInfo(Movie[] movie){
        for (int i = 0; i < movie.length; i++) {
            System.out.println(i + " - " + movie[i].getName() + " - " + movie[i].getGenre());
        }
        System.out.print(INPUT_TO_SEE_INFO);
        do {
            st = scanner.nextLine();
            if(st.equals("0")){
                System.out.println(movie[0].getScript());
            }if(st.equals("1")){
                System.out.println(movie[1].getScript());
            }if(st.equals("2")){
                System.out.println(movie[2].getScript());
            }if(st.equals("3")){
                break;
            }else{
                System.out.print(INPUT_TO_SEE_INFO);
            }
        }while (!st.equals("0") || !st.equals("1") || !st.equals("2") || !st.equals("3"));
    }

    //Method create movie;
    public void makeMovie(Actor[] actors, Movie[] movies, Director[] director, Cameraman[] cameramen){
        System.out.println("Movie available for make:");
        for (int i = 0; i < movies.length; i++) {
            System.out.println(i + " - " + movies[i].getName());
        }
        System.out.print(INPUT_TO_SEE_INFO);
        do {
            st = scanner.nextLine();
            if(st.equals("0")){
                System.out.println(movies[0].getName() + READY);
                director[0].participateInMovie(movies[0]);
                director[1].participateInMovie(movies[0]);
                director[2].participateInMovie(movies[0]);

                actors[0].participateInMovie(movies[0]);
                actors[1].participateInMovie(movies[0]);
                actors[2].participateInMovie(movies[0]);

                cameramen[0].participateInMovie(movies[0]);
                cameramen[1].participateInMovie(movies[0]);
                cameramen[2].participateInMovie(movies[0]);
            }if(st.equals("1")){
                System.out.println(movies[1].getName() + READY);
                director[0].participateInMovie(movies[1]);
                director[1].participateInMovie(movies[1]);
                director[2].participateInMovie(movies[1]);

                actors[0].participateInMovie(movies[1]);
                actors[1].participateInMovie(movies[1]);
                actors[2].participateInMovie(movies[1]);

                cameramen[0].participateInMovie(movies[1]);
                cameramen[1].participateInMovie(movies[1]);
                cameramen[2].participateInMovie(movies[1]);
            }if(st.equals("2")){
                System.out.println(movies[2].getName() + READY);
                director[0].participateInMovie(movies[2]);
                director[1].participateInMovie(movies[2]);
                director[2].participateInMovie(movies[2]);

                actors[0].participateInMovie(movies[2]);
                actors[1].participateInMovie(movies[2]);
                actors[2].participateInMovie(movies[2]);

                cameramen[0].participateInMovie(movies[2]);
                cameramen[1].participateInMovie(movies[2]);
                cameramen[2].participateInMovie(movies[2]);
            }
            if (st.equals("3")){
                break;
            }else {
                System.out.print(INPUT_TO_SEE_INFO);
            }
        }while (!st.equals("0") || !st.equals("1") || !st.equals("2") || !st.equals("3"));
    }

}