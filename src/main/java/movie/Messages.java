package movie;

public class Messages {

    public static final String SHOW_ACTORS = "1 - Show all available actors";
    public static final String SHOW_DIRECTORS = "2 - Show all available directors";
    public static final String SHOW_SCRIPTS = "3 - Read the scripts";
    public static final String START_MAKING_MOVIE = "4 - Take a movie";
    public static final String EXIT = "5 - Exit";
    public static final String INPUT_NUMBER = "Input number: ";
    public static final String INTERSTELLAR = "The crew consists of Cooper, the robots TARS and CASE, and the scientists Dr. Amelia Brand (Professor Brand's daughter), Dr. Romilly, and Dr. Doyle. \n" +
            "After traversing the wormhole, Cooper, Doyle and Amelia use a Ranger to investigate Miller's planet, an ocean world. \n" +
            "After landing in knee-high water and finding only wreckage from Miller's expedition, a gigantic tidal wave kills Doyle and delays departure. \n" +
            "Due to the proximity to the black hole, time is severely dilated; 23 years have elapsed on Endurance by the time they return.";
    public static final String AVENGERS = "In Stuttgart, Barton steals iridium needed to stabilize the Tesseract's power while Loki causes a distraction, \n" +
            "leading to a brief confrontation with Rogers, Stark, and Romanoff that ends with Loki's surrender. While Loki is being escorted to S.H.I.E.L.D., \n" +
            "Thor, his adoptive brother, arrives and frees him, hoping to convince him to abandon his plan and return to Asgard. After a confrontation with Stark and Rogers, \n" +
            "Thor agrees to take Loki to S.H.I.E.L.D.'s flying aircraft carrier, the Helicarrier. Upon arrival, Loki is imprisoned while Banner and Stark attempt to locate the Tesseract.";
    public static final String INSIDIOUS = "Married couple Josh and Renai Lambert, their children Dalton, Foster, and infant daughter Cali have recently moved into a new home. One night, \n" +
            "Dalton is drawn to the attic when he hears creaking noises and sees the door open by itself. He falls from a ladder while investigating and sees a figure in the shadows. \n" +
            "Hearing his terrified screams, Renai and Josh rush to the boy's aid. The next day, Dalton falls into an inexplicable coma.";

    public static final String INPUT_TO_SEE_INFO = "Input number to see information about movie or 3 to back in menu: ";
    public static final String READY = " - ready to make!";

}