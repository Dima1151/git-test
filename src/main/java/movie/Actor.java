package movie;

public class Actor extends MovieCrewMembers implements ScriptKnowlege{

    private boolean isMainRole;
    Script script;
    String genre;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean getMainRole() {
        return isMainRole;
    }

    public void setMainRole(boolean mainRole) {
        isMainRole = mainRole;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    @Override
    public void participateInMovie(Movie movie) {
        if(movie.getGenre() == this.getGenre()){
            setMainRole(true);
            if(getMainRole()){
                System.out.println("This actor will be playing in this movie: " + getFirstName() + " " + getLastName());
            }
        }
    }

}
