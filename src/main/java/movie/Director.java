package movie;

public class Director extends MovieCrewMembers implements ScriptKnowlege {

    Script script;
    String genre;

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Script getScript() {
        return script;
    }

    public void setScript(Script script) {
        this.script = script;
    }

    @Override
    public void participateInMovie(Movie movie) {
        if(movie.getGenre() == this.getGenre()){
            System.out.println("Movie will be directed by: " + getFirstName() + " " + getLastName());
        }
    }

}