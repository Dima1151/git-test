package movie;

import java.util.Scanner;

import static movie.Messages.*;

public class MovieSet {

    public static void main(String[] args) {

        //Give name, genre and script to our movies
        Movie interstellar = new Movie();
        interstellar.setName("Interstellar");
        interstellar.setGenre("Sci-Fi");
        interstellar.setScript(INTERSTELLAR);

        Movie avengers = new Movie();
        avengers.setName("Avengers");
        avengers.setGenre("Fantasy");
        avengers.setScript(AVENGERS);

        Movie insidious = new Movie();
        insidious.setName("Insidious");
        insidious.setGenre("Horror");
        insidious.setScript(INSIDIOUS);

        //Directories who will direct our movie
        Director director = new Director();
        director.setFirstName("James");
        director.setLastName("Wan");
        director.setGenre("Horror");

        Director director1 = new Director();
        director1.setFirstName("Steven");
        director1.setLastName("Spielberg");
        director1.setGenre("Sci-Fi");

        Director director2 = new Director();
        director2.setFirstName("Joss");
        director2.setLastName("Whedon");
        director2.setGenre("Fantasy");

        //Actors who will be playing in movie
        Actor actor = new Actor();
        actor.setFirstName("Robert");
        actor.setLastName("Downey Jr.");
        actor.setGenre("Fantasy");
        actor.setMainRole(false);

        Actor actor1 = new Actor();
        actor1.setFirstName("Lin");
        actor1.setLastName("Shaye");
        actor1.setGenre("Horror");
        actor1.setMainRole(false);

        Actor actor2 = new Actor();
        actor2.setFirstName("Matthew");
        actor2.setLastName("McConaughey");
        actor2.setGenre("Sci-Fi");
        actor2.setMainRole(false);

        //Personal who will be operating movie
        Cameraman cameraman = new Cameraman();
        cameraman.setFirstName("Adam");
        cameraman.setLastName("Smith");
        cameraman.setCameraGenre("Fantasy");

        Cameraman cameraman1 = new Cameraman();
        cameraman1.setFirstName("Erlich");
        cameraman1.setLastName("Bachman");
        cameraman1.setCameraGenre("Horror");

        Cameraman cameraman2 = new Cameraman();
        cameraman2.setFirstName("Rick");
        cameraman2.setLastName("Grimes");
        cameraman2.setCameraGenre("Sci-Fi");

        //array of actors
        Actor[] actors = new Actor[3];
        actors[0] = actor;
        actors[1] = actor1;
        actors[2] = actor2;

        //array of directors
        Director[] directors = new Director[3];
        directors[0] = director;
        directors[1] = director1;
        directors[2] = director2;

        //array of personal
        Cameraman[] cameramen = new Cameraman[3];
        cameramen[0] = cameraman;
        cameramen[1] = cameraman1;
        cameramen[2] = cameraman2;

        //array if movie
        Movie[] movies = new Movie[3];
        movies[0] = interstellar;
        movies[1] = avengers;
        movies[2] = insidious;

        ShowInfo showInfo = new ShowInfo();

        Scanner scanner = new Scanner(System.in);
        String st;

        System.out.println(SHOW_ACTORS + "\n" + SHOW_DIRECTORS + "\n" + SHOW_SCRIPTS + "\n" + START_MAKING_MOVIE + "\n" + EXIT);
        System.out.print(INPUT_NUMBER);
        do {
            st = scanner.nextLine();
            if(st.equals("1")){
                showInfo.showActorInfo(actors);
            }if(st.equals("2")){
                showInfo.showDirectorInfo(directors);
            }if(st.equals("3")){
                showInfo.showMovieInfo(movies);
            }if(st.equals("4")){
                showInfo.makeMovie(actors, movies, directors, cameramen);
            }if(st.equals("5")){
                break;
            }else {
                System.out.print(INPUT_NUMBER);
            }
        }while (!st.equals("1") || !st.equals("2") || !st.equals("3") || !st.equals("4"));

    }

}