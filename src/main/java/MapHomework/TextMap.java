package MapHomework;

import java.util.HashMap;
import java.util.Set;

public class TextMap {

    public static void main(String[] args) {

        String text1 = "In the dark, dark wood, there was a dark, dark house the dark, dark wood, there was a dark, dark house the dark, dark wood, there was a dark, dark house the dark, dark wood, there was a dark, dark house the dark, dark wood, there was a dark, dark house.";
        text1.toLowerCase();

        text(text1);
    }

    static void text(String text){
        HashMap<String, Integer> hashMap = new HashMap<>();
        String[] st;
        st = text.split("\\,\\s|\\s|\\-|\\!\\s|\\.");


        for (int i = 0; i < st.length; i++) {
            if(hashMap.containsKey(st[i])){
                hashMap.put(st[i], hashMap.get(st[i]) + 1);
            }else{
                hashMap.put(st[i].toLowerCase(), 1);
            }
        }

        Set<String> keys = hashMap.keySet();

        for (String key : keys) {
            System.out.println(key + " - " + hashMap.get(key));
        }
    }
}
