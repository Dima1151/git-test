package MapHomework.LinkedShop;

public abstract class Abstract {
    public abstract void pickCategory();
    public abstract void laptops();
    public abstract void smartphones();
    public abstract void tablets();
}
