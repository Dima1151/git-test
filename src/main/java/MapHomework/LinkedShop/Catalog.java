package MapHomework.LinkedShop;

public class Catalog {

    String name;
    int ram;
    String OS;

    public Catalog(String name, int ram, String OS) {
        this.name = name;
        this.ram = ram;
        this.OS = OS;
    }

    public String info(){
        return name + " " + ram + "Gb " + OS;
    }

}
