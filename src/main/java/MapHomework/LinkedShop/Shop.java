package MapHomework.LinkedShop;

import java.util.LinkedList;
import java.util.Scanner;

import static MapHomework.LinkedShop.Message.*;


public class Shop extends Abstract{

    String code;
    Scanner sc = new Scanner(System.in);

    @Override
    public void pickCategory() {

        System.out.println(WELOCME);
        catalog();
        System.out.print(PICK_CATEGORY);

        do {
            code = sc.nextLine();
            if(code.equals("1")){
                laptops();
            }else if(code.equals("2")){
                smartphones();
            }else if(code.equals("3")){
                tablets();
            }else if(code.equals("4")){
                break;
            }else{
                System.out.print(PICK_CATEGORY);
            }
        }while (!code.equals("1") || !code.equals("2") || !code.equals("3") || !code.equals("4"));

    }

    static void catalog(){

        LinkedList<String> catalogShop = new LinkedList<>();

        String[] catalog = {"1 - Laptops", "2 - Smartphones", "3 - Tablets", "4 - Exit"};

        for (String catal:catalog) {
            catalogShop.add(catal);
        }
        for (int i = 0; i < catalogShop.size(); i++) {
            System.out.println(catalogShop.get(i));
        }

    }

    @Override
    public void laptops(){

        Catalog lenovo = new Catalog("Lenove Ideapad 720s Touch", 8, "Windows");
        Catalog hp = new Catalog("HP 14-df0000", 8, "Windows");
        Catalog apple = new Catalog("MacBook Pro", 16, "macOS");

        String[] laptop = new String[3];
        laptop[0] = lenovo.info();
        laptop[1] = hp.info();
        laptop[2] = apple.info();

        LinkedList<String> laptops = new LinkedList<>();

        for (String lap:laptop) {
            laptops.add(lap);
        }
        for (int i = 0; i < laptops.size(); i++) {
            System.out.println(laptops.get(i));
        }
        System.out.print(BACK_BUTTON);
        do {
            code = sc.nextLine();
            if(code.equals("1")){
                break;
            }else{
                System.out.print(BACK_BUTTON);
            }
        }while (!code.equals("1"));

    }

    @Override
    public void smartphones() {

        Catalog samsung = new Catalog("Samsung galaxy s9", 4, "Android 8.0 (Oreo)");
        Catalog iphone = new Catalog("Iphone XS", 4, "iOS 12");
        Catalog asus = new Catalog("Asus ZenFone 5", 4, "Android 7.0 (Nougat)");

        String[] smartphones = new String[3];
        smartphones[0] = samsung.info();
        smartphones[1] = iphone.info();
        smartphones[2] = asus.info();

        LinkedList<String> smartphone = new LinkedList<>();

        for (String smart:smartphones) {
            smartphone.add(smart);
        }
        for (int i = 0; i < smartphone.size(); i++) {
            System.out.println(smartphone.get(i));
        }
        System.out.print(BACK_BUTTON);
        do {
            code = sc.nextLine();
            if(code.equals("1")){
                break;
            }else{
                System.out.print(BACK_BUTTON);
            }
        }while (!code.equals("1"));

    }

    @Override
    public void tablets() {

        Catalog samsung = new Catalog("Samsung Galaxy Tab S4", 4, "Android 8.0 (Oreo)");
        Catalog ipad = new Catalog("iPad Pro", 4, "iOS 12");
        Catalog asus = new Catalog("Asus ZenPad 10", 2, "Android 7.0 (Nougat)");

        String[] tablets = new String[3];
        tablets[0] = samsung.info();
        tablets[1] = ipad.info();
        tablets[2] = asus.info();

        LinkedList<String> tablet = new LinkedList<>();

        for (String tab:tablets) {
            tablet.add(tab);
        }
        for (int i = 0; i < tablet.size(); i++) {
            System.out.println(tablet.get(i));
        }
        System.out.print(BACK_BUTTON);
        do {
            code = sc.nextLine();
            if(code.equals("1")){
                break;
            }else{
                System.out.print(BACK_BUTTON);
            }
        }while (!code.equals("1"));

    }
}
