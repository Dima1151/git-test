package Pair;

public class Pair<T, E> {

    private T t;
    private E e;

    public Pair(T t, E e) {
        this.t = t;
        this.e = e;
    }

    public T getT() {
        return (T) e;
    }

    public E getE() {
        return (E) t;
    }

    @Override
    public String toString() {
        return "Pair{" +
                "t=" + e +
                ", e=" + t +
                '}';
    }

    public static void main(String[] args) {

        Pair<Integer, Integer> pair = new Pair<Integer, Integer>(84,35);
        System.out.println(pair.getE());
        System.out.println(pair.toString());

        Pair<String, String> pair1 = new Pair<String, String>("Hello", "World");
        System.out.println(pair1.toString());
    }
}
