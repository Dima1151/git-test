package registration;

public class UserConstants {

    //Strings for userName;
    public static final String REGEX_USERNAME = "^[a-z0-9._-]{3,16}$";
    public static final String INPUT_USERNAME = "Input username: ";
    public static final String INCORRECT_USERNAME = "You are inputed incorrect username. Pleace try agein!";

    //Strings for password;
    public static final String REGEX_PASSWORD = "^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,18}$";
    public static final String INPUT_PASSWORD = "Input password: ";
    public static final String INCORRECT_PASSWORD = "You are inputed incorrect password. Pleace try agein!";

    //Strings for email;
    public static final String REGEX_EMAIL = "^((([!#$%&'*+\\-/=?^_`{|}~\\w])|([!#$%&'*+\\-/=?^_`{|}~\\w][!#$%&'*+\\-/=?^_`{|}~\\.\\w]{0,}[!#$%&'*+\\-/=?^_`{|}~\\w]))[@]\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)$";
    public static final String INPUT_EMAIL = "Input email: ";
    public static final String INCORRECT_EMAIL = "You are inputed incorrect email. Pleace try agein!";

    //String for name and surname;
    public static final String REGEX_NAMEandSURNAME = "^([a-zA-Z]+[\\'\\,\\.\\-]?[a-zA-Z ]*)+[ ]([a-zA-Z]+[\\'\\,\\.\\-]?[a-zA-Z ]+)+$";
    public static final String INPUT_NAMEandSURNAME = "Input your name and surname: ";
    public static final String INCORRECT_NAMEandSURNAME = "You are inputed incorrect name or surname. Pleace try agein!";

}
