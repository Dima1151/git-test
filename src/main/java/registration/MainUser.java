package registration;

import java.util.Scanner;

import static registration.UserConstants.*;

public class MainUser {

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);

        String username = readWithRetry(in, REGEX_USERNAME, INPUT_USERNAME, INCORRECT_USERNAME);
        String password = readWithRetry(in, REGEX_PASSWORD, INPUT_PASSWORD, INCORRECT_PASSWORD);
        String email = readWithRetry(in, REGEX_EMAIL, INPUT_EMAIL, INCORRECT_EMAIL);
        String nameAndSurname = readWithRetry(in, REGEX_NAMEandSURNAME, INPUT_NAMEandSURNAME, INCORRECT_NAMEandSURNAME);

        Registration registration = new Registration();
        registration.newUser(username, password, email, nameAndSurname);

    }

    private static String readWithRetry(Scanner scanner, String regex, String messageToShow, String errorMessage) {
        while (true) {
            System.out.println(messageToShow);
            String input = scanner.nextLine();
            if (input.matches(regex)) {
                return input;
            } else {
                System.out.println(errorMessage);
            }
        }
    }
}

