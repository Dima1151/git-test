package registration;

import java.io.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Registration {

    public void newUser(String username, String password, String email, String nameSurname){

        createFolder("/home/dima/IdeaProjects/git-test/src/main/java/registration/all-users");
        createFile("/home/dima/IdeaProjects/git-test/src/main/java/registration/all-users/users.txt");
        Set<User> users = readFromFile("/home/dima/IdeaProjects/git-test/src/main/java/registration/all-users/users.txt");
        check("/home/dima/IdeaProjects/git-test/src/main/java/registration/all-users/users.txt", new User(username, password, email, nameSurname), users);
        see(users);
    }

    private File createFolder(String folder){
        File file = new File(folder);
        if(!file.exists()){
            if(file.mkdir()){
                System.out.println("Inventory directory was created!");
            }else{
                System.out.println("Failed to created directory!");
            }
        }
        return new File(folder);
    }

    private File createFile(String filetxt){
        File file = new File(filetxt);
        if(!file.exists()){
            try {
                file.createNewFile();
                System.out.println("File was created");
            }catch (IOException e){
                e.printStackTrace();
            }
        }else {
            System.out.println("Failed to created files!");
        }
        return new File(filetxt);
    }

    private void see(Set<User> users){
        Iterator iterator = users.iterator();
        while (iterator.hasNext()){
            System.out.println("User: " + iterator.next());
        }
    }

    private File writeFile(String file, Set<User> users){
        try(FileOutputStream fileOutputStream = new FileOutputStream(file)){
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(users);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File(file);
    }

    private Set<User> check(String file, User user, Set<User> users){
        if(users.size() == 0 || !users.contains(user)){
            users.add(user);
            writeFile(file, users);
            System.out.println("Successful");
            return users;
        }else{
            System.out.println("Failed");
        }
            return users;
    }

    private Set<User> readFromFile(String file){
        Set<User> users = new HashSet<>();
        try(FileInputStream fileInputStream = new FileInputStream(file)){
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            users = (HashSet<User>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users;
    }
}
