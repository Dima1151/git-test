package inventoryShop;

import java.util.Objects;

public class Laptops {

    private String name;
    private Integer screen;
    private Integer price;

    public Laptops(String name, Integer screen, Integer price) {
        this.name = name;
        this.screen = screen;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScreen() {
        return screen;
    }

    public void setScreen(Integer screen) {
        this.screen = screen;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laptops laptops = (Laptops) o;
        return screen == laptops.screen &&
                price == laptops.price &&
                Objects.equals(name, laptops.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, screen, price);
    }

    @Override
    public String toString() {
        return name + "|" + screen + "|" + price;
    }
}
