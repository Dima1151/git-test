package inventoryShop;

import java.io.*;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class InventoryManager{

    public void makeInventoryFilesByPrice(String inventoryFilePath){
        Set<Laptops> laptops = getLaptopsSetFromFile(new File(inventoryFilePath));
//        System.out.println(laptops);
        createFolder("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory/laptops");
        File under1500LaptopsFile = createFile("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory/laptops/under1500LaptopsFile.txt");
        File over1500LaptopsFile = createFile("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory/laptops/over1500LaptopsFile.txt");
        analyze(laptops, under1500LaptopsFile, over1500LaptopsFile);
    }

    private Set<Laptops> getLaptopsSetFromFile(File file){
        Set<Laptops> laptops = new HashSet<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()){
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter("\\|");

                String name = lineScanner.next();
                String screen = lineScanner.next();
                String price = lineScanner.next();
                Laptops item = new Laptops(name, Integer.valueOf(screen), Integer.valueOf(price));
                laptops.add(item);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return laptops;
    }

    private File createFolder(String FolderPath){
        File file = new File("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory");
        File file1 = new File("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory/laptops");
        if(!file.exists()){
            if(file.mkdirs()){
                System.out.println("Inventory directory was created!");
                if(!file1.exists()){
                    if(file1.mkdirs()){
                        System.out.println("Laptops directory was created!");
                    }
                }
            }else{
                System.out.println("Failed to created directory!");
            }
        }
        return new File(FolderPath);
    }

    private File createFile(String FilePath) {
        File under = new File("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory/laptops/under1500LaptopsFile.txt");
        File over = new File("/home/dima/IdeaProjects/git-test/src/main/java/inventoryShop/inventory/laptops/over1500LaptopsFile.txt");
        if (!under.exists()) {
            try {
                under.createNewFile();
                over.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Failed to created files!");
        }
        return new File(FilePath);
    }

    public void analyze(Set<Laptops> laptops, File under1500LaptopsFile, File over1500LaptopsFile){
        PrintStream under1500 = null;
        PrintStream over1500 = null;
        try{
            under1500 = new PrintStream(under1500LaptopsFile);
            over1500 = new PrintStream(over1500LaptopsFile);
            for(Laptops laptop: laptops) {
                if(laptop.getPrice() < 1500) {
                    under1500.println(laptop);
                } else {
                    over1500.println(laptop);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally {
            under1500.close();
            over1500.close();
        }
    }
}
