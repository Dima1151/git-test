import src.main.java.httpHomework.Person;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class SignUp extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        String firstName = request.getParameter("First name");
        String lastName = request.getParameter("Last name");
        String email = request.getParameter("Email");
        String password = request.getParameter("Password");

        Person person = new Person(firstName, lastName, email, password);
        newUser(person);

        out.println(
                "<html>" +
                        "<head><title>Example</title></head>" +
                        "<body>" +
                        "<h1>Create new account</h1>" +
                        "First name = " + firstName +
                        " Last name = " + lastName +
                        " Email = " + email +
                        " Password = " + password +
                        "<form action='signup' method='get'>" +
                        "</br>" +
                        "<input type='text' name='First name' placeholder='First name'/></br>" +
                        "<input type='text' name='Last name' placeholder='Last name'/></br>" +
                        "<input type='text' name='Email' placeholder='Email'/></br>" +
                        "<input type='password' name='Password' placeholder='Password'/></br>" +
                        "<input type='submit' name='button' value='Sigh up'/>" +
                        "</form>" +
                        "</body>" +
                        "</html>"
        );
    }

    public void newUser(Person person){
        Set<Person> users = readFromFile("/home/dima/IdeaProjects/logInHome/src/main/info");
        check("/home/dima/IdeaProjects/logInHome/src/main/info", person, users);
    }

    private File writeFile(String file, Set<Person> users){
        try(FileOutputStream fileOutputStream = new FileOutputStream(file)){
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

            objectOutputStream.writeObject(users);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return new File(file);
    }

    private Set<Person> check(String file, Person user, Set<Person> users){
        if(users.size() == 0 || !users.contains(user)){
            users.add(user);
            writeFile(file, users);
            return users;
        }else{
            System.out.println("Failed");
        }
        return users;
    }

    private Set<Person> readFromFile(String file){
        Set<Person> users = new HashSet<>();
        try(FileInputStream fileInputStream = new FileInputStream(file)){
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            users = (HashSet<Person>) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return users;
    }

}
